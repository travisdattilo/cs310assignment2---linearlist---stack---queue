package data_structures;

public class QueueGrader {
    private Queue<Integer> list;
    
    public QueueGrader() {
        list = new Queue<Integer>();
        runTests();
    }
    //enqueue, dequeue, size, isEmpty, peek, contains, makeEmpty, remove, iterator
    private void runTests() {
    	for(int i = 200; i > 0; i--)
        	list.enqueue(i);
        print("Should print 200 .. 1");
        print();
        
        list.dequeue();
        print("Now prints 199 .. 1");
        print();
        print("");
     
        print("Size is 199:                              " + list.size());
        print("isEmpty should be false:                  " + list.isEmpty());
        print("Peek at element at front of queue is 199: " + list.peek());
        print("Contains a 18  should return true:        " + list.contains(18));
        print("Contains a 100 should return true:        " + list.contains(100));
        print("Contains a 146 should return true:        " + list.contains(146));
        print("Contains a 73  should return true:        " + list.contains(73));
        print("Contains a 300 should return false:       " + list.contains(300));
        print("Contains a 200 should return false:       " + list.contains(200));
         
        print("");
        
        print("Removes a 177 from the list:");
        list.remove(177);
        print();
        print("Removes a 100 from the list:");
        list.remove(100);
        print();
        print("Contains a 100 should return false:   " + list.contains(100));
        print("Contains a 177   should return false: " + list.contains(177));
        
        print("");
        print("makeEmpty deletes the whole list and following line should be empty:");
        list.makeEmpty();
        print();
        print("There should be no list above this line");
        print("isEmpty should be true: " + list.isEmpty());
        print("Size should be 0: " + list.size());
        print("Enqueue a 10, 26, and 4, should print out 10 26 4:");
        list.enqueue(10);
        list.enqueue(26);
        list.enqueue(4);
        print();
        print("Size should be 3: " + list.size());
        print("Dequeue once should leave 26 4:");
        list.dequeue();
        print();
        print("Size should be 2: " + list.size());
    }
    
    private void print(String s) {
        System.out.println(s);
        }
       
    private void print() {
        for(Integer i : list)
            System.out.print(i + " ");
        System.out.println();
        }
    
    public static void main(String [] args) {
        new QueueGrader();
    }
}
