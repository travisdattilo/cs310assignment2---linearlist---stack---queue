package data_structures;

public class StackGrader {
    private Stack<Integer> list;
    
    public StackGrader() {
        list = new Stack<Integer>();
        runTests();
    }
    //push, pop, size, isEmpty, peek, contains, makeEmpty, remove, iterator
    private void runTests() {
        for(int i = 200; i > 0; i--)
        	list.push(i);
        print("Should print 1 .. 200");
        print();
        
        list.pop();
        print("Now prints 2 .. 200");
        print();
        print("");
        
        print("Size is 199:                          " + list.size());
        print("isEmpty should be false:              " + list.isEmpty());
        print("Peek at element at top of stack is 2: " + list.peek());
        print("Contains a 18  should return true:    " + list.contains(18));
        print("Contains a 100 should return true:    " + list.contains(100));
        print("Contains a 146 should return true:    " + list.contains(146));
        print("Contains a 73  should return true:    " + list.contains(73));
        print("Contains a 300 should return false:   " + list.contains(300));
        print("Contains a 1   should return false:   " + list.contains(1));
        
        print("");
        
        print("Removes a 3 from the list:");
        list.remove(3);
        print();
        print("Removes a 100 from the list:");
        list.remove(100);
        print();
        print("Contains a 100 should return false: " + list.contains(100));
        print("Contains a 3   should return false: " + list.contains(3));
        
        print("");
        print("makeEmpty deletes the whole list and following line should be empty:");
        list.makeEmpty();
        print();
        print("There should be no list above this line");
        print("isEmpty should be true: " + list.isEmpty());
        print("Size should be 0: " + list.size());
        print("Pushing a 10, 26, and 4, should print out 4 26 10:");
        list.push(10);
        list.push(26);
        list.push(4);
        print();
        print("Size should be 3: " + list.size());
        print("Popping once should leave 26 10:");
        list.pop();
        print();
        print("Size should be 2: " + list.size());
    }
    
    private void print(String s) {
        System.out.println(s);
        }
       
    private void print() {
        for(Integer i : list)
            System.out.print(i + " ");
        System.out.println();
        }
    
    public static void main(String [] args) {
        new StackGrader();
    }
}
